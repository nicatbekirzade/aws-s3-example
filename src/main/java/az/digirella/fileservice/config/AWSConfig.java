package az.digirella.fileservice.config;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AWSConfig {

    @Value("${minio.access.name}")
    private String accesskey;

    @Value("${minio.access.secret}")
    private String secretkey;

    @Value("${minio.endPoint}")
    private String endPoint;

    public AWSCredentials credentials() {
        return new BasicAWSCredentials(
                accesskey,
                secretkey
        );
    }

    @Bean
    public AmazonS3 amazonS3() {

        ClientConfiguration clientConfig = new ClientConfiguration().withRequestTimeout(50000);
        clientConfig.setUseTcpKeepAlive(true);

        return AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials()))
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endPoint, Regions.US_EAST_1.getName()))
                .withPathStyleAccessEnabled(true)
                .withClientConfiguration(clientConfig)
                .build();
    }

    @Bean
    public TransferManager transferManager() {
        return TransferManagerBuilder.standard()
                .withS3Client(amazonS3())
                .withMultipartUploadThreshold((long) (5 * 1024 * 1025))
                .build();
    }

}
