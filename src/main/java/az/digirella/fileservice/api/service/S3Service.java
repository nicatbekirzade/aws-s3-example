package az.digirella.fileservice.api.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.CreateBucketRequest;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.transfer.TransferManager;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Service
@RequiredArgsConstructor
public class S3Service {

    private final AmazonS3 s3Client;
    private final TransferManager transferManager;

    public List<Bucket> listBuckets() {
        return s3Client.listBuckets();
    }

    public void createBucket(String bucketName) {
        s3Client.createBucket(new CreateBucketRequest(bucketName).withCannedAcl(CannedAccessControlList.Private));
    }

    public void deleteBucket(String bucketName) {
        s3Client.deleteBucket(bucketName);
    }

    public List<String> listObjects(String bucketName) {
        ObjectListing objectListing = s3Client.listObjects(bucketName);
        return objectListing.getObjectSummaries().stream().map(S3ObjectSummary::getKey).collect(Collectors.toList());
    }

    public void uploadFile(String bucketName, MultipartFile multipartFile) throws IOException {
        ObjectMetadata data = new ObjectMetadata();
        data.setContentType(multipartFile.getContentType());
        data.setContentLength(multipartFile.getSize());
        PutObjectResult objectResult = s3Client.putObject(bucketName, multipartFile.getOriginalFilename(), multipartFile.getInputStream(), data);
        System.out.println(objectResult.getContentMd5()); //you can verify MD5
    }

    public InputStream downloadObject(String bucketName, String objectName) {
        S3Object s3object = s3Client.getObject(bucketName, objectName);
        S3ObjectInputStream inputStream = s3object.getObjectContent();

        return inputStream;
//        try {
//            FileUtils.copyInputStreamToFile(inputStream, new File(objectName));
//        } catch (IOException e) {
//            log.error(e.getMessage());
//        }
    }

    public void moveObject(String bucketSourceName, String objectName, String bucketTargetName) {
        s3Client.copyObject(
                bucketSourceName,
                objectName,
                bucketTargetName,
                objectName
        );
    }

    public void deleteObject(String bucketName, String objectName) {
        s3Client.deleteObject(bucketName, objectName);
    }

    public void deleteMultipleObjects(String bucketName, List<String> objects) {
        DeleteObjectsRequest delObjectsRequests = new DeleteObjectsRequest(bucketName)
                .withKeys(objects.toArray(new String[0]));
        s3Client.deleteObjects(delObjectsRequests);
    }
}
