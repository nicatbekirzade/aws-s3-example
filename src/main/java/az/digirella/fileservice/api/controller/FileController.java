package az.digirella.fileservice.api.controller;

import az.digirella.fileservice.api.service.S3Service;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.util.IOUtils;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/buckets")
public class FileController {

    private final S3Service s3Service;

    @GetMapping
    public List<String> listBuckets() {
        var buckets = s3Service.listBuckets();
        return buckets.stream().map(Bucket::getName).collect(Collectors.toList());
    }

    @PostMapping(value = "/{bucketName}")
    public void createBucket(@PathVariable String bucketName) {
        s3Service.createBucket(bucketName);
    }

    @DeleteMapping(value = "/{bucketName}")
    public void deleteBucket(@PathVariable String bucketName) {
        s3Service.deleteBucket(bucketName);
    }

    @GetMapping(value = "/{bucketName}/objects")
    public List<String> listObjects(@PathVariable String bucketName) {
        return s3Service.listObjects(bucketName);
    }

    @PostMapping(value = "/{bucketName}/objects")
    public ResponseEntity<Object> upload(@PathVariable String bucketName, @RequestPart MultipartFile file) throws IOException {
        s3Service.uploadFile(bucketName, file);
        return ResponseEntity.ok("sdf");
    }

//    @GetMapping(value = "/{bucketName}/objects/{objectName}") //FIXME
//    public File downloadObject(@PathVariable String bucketName, @PathVariable String objectName) {
//        s3Service.downloadObject(bucketName, objectName);
//        return new File(objectName);
//    }

    @CrossOrigin("*")
    @GetMapping(
            value = "/{bucketName}/objects/{objectName}/byte",
            produces = MediaType.APPLICATION_PDF_VALUE
    )
    public byte[] getFileAsByte(@PathVariable String bucketName, @PathVariable String objectName) throws IOException {
        InputStream in = s3Service.downloadObject(bucketName, objectName);
        return IOUtils.toByteArray(in);
    }

    @CrossOrigin("*")
    @GetMapping(
            value = "/{bucketName}/objects/{objectName}"
//            ,produces = MediaType.APPLICATION_PDF_VALUE
    )
    public String getFile(@PathVariable String bucketName, @PathVariable String objectName) throws IOException {
        InputStream in = s3Service.downloadObject(bucketName, objectName);
        return Base64.getEncoder().encodeToString(IOUtils.toByteArray(in));
    }

    @PatchMapping(value = "/{bucketSourceName}/objects/{objectName}/{bucketTargetName}")
    public void moveObject(@PathVariable String bucketSourceName, @PathVariable String objectName, @PathVariable String bucketTargetName) {
        s3Service.moveObject(bucketSourceName, objectName, bucketTargetName);
    }

    @DeleteMapping(value = "/{bucketName}/objects/{objectName}")
    public void deleteObject(@PathVariable String bucketName, @PathVariable String objectName) {
        s3Service.deleteObject(bucketName, objectName);
    }

    @DeleteMapping(value = "/{bucketName}/objects")
    public void deleteObject(@PathVariable String bucketName, @RequestBody List<String> objects) {
        s3Service.deleteMultipleObjects(bucketName, objects);
    }

}
